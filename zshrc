export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME=""
CASE_SENSITIVE="true"
DISABLE_AUTO_UPDATE="true"
HISTFILE=~/.histfile
HISTSIZE=50000
SAVEHIST=50000
# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
plugins=(git pip zsh-completions)
fpath=($ZSH/custom/plugins/zsh-completions/src $fpath)

ZSH_CACHE_DIR="${XDG_CACHE_HOME:-$HOME/.cache}/.oh-my-zsh-cache"
mkdir -p "$ZSH_CACHE_DIR"

autoload -Uz compinit promptinit colors zargs
source $ZSH/oh-my-zsh.sh
compinit
promptinit
colors

shellbgcolor=blue
shellfgcolor=white

bindkey -v
bindkey '^r' history-incremental-search-backward
bindkey '^q' push-line
typeset -g -A key
#terminal
bindkey ';5D' backward-word
bindkey ';5C' forward-word
bindkey '\e[H' vi-beginning-of-line
bindkey '\e[F' vi-end-of-line
bindkey '\e[3' delete-char
#konsola
key[Home]=${terminfo[khome]}
key[End]=${terminfo[kend]}
key[Delete]=${terminfo[kdch1]}
[[ -n "${key[Home]}"    ]]  && bindkey  "${key[Home]}"    vi-beginning-of-line
[[ -n "${key[End]}"     ]]  && bindkey  "${key[End]}"     vi-end-of-line
[[ -n "${key[Delete]}"  ]]  && bindkey  "${key[Delete]}"  delete-char

USERAGENT_FIREFOX='User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0'

alias lsm="ls -1UN --color=never"
alias ls="ls --color"
alias mv="mv -i"
alias fanfary="play ~/alarm.ogg > /dev/null 2> /dev/null"
alias free='free -h'
alias nl='nl -b a'

function git-readd()
{
    git diff "${1}~" "${1}" | git apply
}

function mkcd()
{
    mkdir -p "$1"
    cd "$1"
}
function cdtmp()
{
    cd $(mktemp -d)
}
function gitk() {
	if ! [ "$*" ]; then
		local maxdepth=$(git rev-list --count --first-parent @~)
		if [ $maxdepth -gt 100 ]; then
			exec gitk HEAD~100..HEAD
		fi
	fi
	command gitk "$@"
}

PROMPT="%{$bg[$shellbgcolor]$fg[black]%} %~ %{$reset_color%}    %T %(?::%{$fg[yellow]%}%?%{$reset_color%})
"
PS2="+= "
ZSH_THEME_TERM_TITLE_IDLE="%~"

function zle-line-init zle-keymap-select {
	local vimode="${${KEYMAP/vicmd/[NORMAL]}/main/[INSERT]}"
	local venv=${VIRTUAL_ENV:+ ${VIRTUAL_ENV#${VIRTUAL_ENV%/*/venv}/}}
	if [ -z "$venv" ]; then
		if [ -d venv ] || [ -e requirements.txt ]; then
			venv=" /bin/python"
		fi
	fi
	PROMPT="%{$bg[$shellbgcolor]$fg[$shellfgcolor]%} %~ %{$reset_color%} %(?::%{$fg[yellow]%}%?%{$reset_color%}) ${(l:8:: :)vimode}${venv}
"
    zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select
export KEYTIMEOUT=1

autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search

setopt appendhistory autocd beep histignorealldups histignorespace
unsetopt share_history
