#!/bin/sh

if [ "$1" ]; then
	arg="$1"
else
	arg="-ekran"
fi

data=`date "+%A, %e %B %Y, %k:%M:%S"`
nazwa="zrzut-${data}.png"

[ $arg = "-ekran" ] && powierzchnia="-window root"
[ $arg = "-okno" ] && powierzchnia="-window $(xdotool getactivewindow) -frame"
[ $arg = "-samookno" ] && powierzchnia="-window $(xdotool getactivewindow)"
[ $arg = "-wybór" ] && powierzchnia=""
[ $arg = "-wybor" ] && powierzchnia=""

import $powierzchnia "$nazwa"
