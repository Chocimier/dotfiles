#!/bin/sh

tools_necessary="
git
rcup
"

error=
for i in $tools_necessary; do
	if ! command -v $i > /dev/null; then
		echo "$i is necessary. Install it first."
		error=1
	fi
done
if [ "$error" ]; then
	exit 1
fi

if command -v zsh >/dev/null; then
	if ! [ -d ~/.oh-my-zsh ]; then
		git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
	fi
	if ! [ -d ~/.oh-my-zsh/custom/plugins/zsh-completions ]; then
		git clone https://github.com/zsh-users/zsh-completions ~/.oh-my-zsh/custom/plugins/zsh-completions
	fi
fi

if command -v nvim >/dev/null; then
	if ! [ -d ~/.local/share/nvim/bundle/Vundle.vim ]; then
		git clone https://github.com/VundleVim/Vundle.vim ~/.local/share/nvim/bundle/Vundle.vim
	fi
fi

# find directory
cd "$(dirname $0)"
dotfiles="$(pwd)"
cd - > /dev/null

RCRC="$dotfiles/rcrc" rcup -d "$dotfiles" "$@"
chmod og-rwx -R ~/.ssh
chmod og-rwx -R "$dotfiles/ssh"
chmod og-rwx "$dotfiles"
